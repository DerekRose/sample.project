﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using STC_TestProject.Models;
using STC_TestProject.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_TestProject.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class APIController : ControllerBase
    {

        private readonly ILogger<APIController> _logger;
        private readonly IRepository<Organization> _orgRepo;
        private readonly IRepository<User> _userRepo;
        private readonly IRepository<ContactInfo> _contactRepo;

        public APIController(ILogger<APIController> logger, IRepository<Organization> orgRepo,
            IRepository<User> userRepo, IRepository<ContactInfo> contactRepo)
        {
            _logger = logger;
            _orgRepo = orgRepo;
            _userRepo = userRepo;
            _contactRepo = contactRepo;
        }

        [HttpGet("organizations/{id}")]
        public Organization Get(Guid id)
        {
            var org = _orgRepo.AsQueryable().FirstOrDefault(p => p.Id == id);
            if (org == null) throw new Exception($"No org found for id: {id}");
            var user = _userRepo.AsQueryable().FirstOrDefault(p => p.OwnerId == org.Id);
            if (user == null) throw new Exception($"No user found for org id: {id}");
            org.Contact = _contactRepo.AsQueryable().FirstOrDefault(p => p.OwnerId == org.Id);
            user.Contact = _contactRepo.AsQueryable().FirstOrDefault(p => p.OwnerId == user.Id);
            return org;
        }

        [HttpPut("organizations/{id}")]
        public Organization Update(Guid id, [FromBody] Organization org)
        {
            throw new NotImplementedException();
        }

        [HttpPost("organizations/")]
        public Organization Create([FromBody] Organization org)
        {
            throw new NotImplementedException();
        }

        [HttpPost("organizations/{id}")]
        public Organization Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
