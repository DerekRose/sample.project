﻿using STC_TestProject.Models;
using STC_TestProject.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_TestProject.Storage
{
    public class UserRepo : IRepository<User>
    {
        //mock data
        public List<User> _data;

        public UserRepo()
        {
            _data = GetSeedData();
        }
        public static Guid GetDefaultUserId() 
        {
            return new Guid(Resources.DefaultUserId);
        }
        public static List<User> GetSeedData()
        {
            return new List<User>
            {
                new User
                {
                    Id = Guid.NewGuid()
                },
                new User
                {
                    OwnerId = OrganizationRepo.GetDefaultOrgId(),
                    Id = GetDefaultUserId(),
                    Name = "Default User"
                },
                new User
                {
                    Id = Guid.NewGuid(),
                }
            };
        }

        public IQueryable<User> AsQueryable()
        {
            throw new NotImplementedException();
        }

        public User Create(User model)
        {
            throw new NotImplementedException();
        }

        public User Delete(User model)
        {
            throw new NotImplementedException();
        }

        public User Retrieve(User model)
        {
            throw new NotImplementedException();
        }

        public User Update(User model)
        {
            throw new NotImplementedException();
        }
    }
}
