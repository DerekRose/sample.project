﻿using System.Linq;

namespace STC_TestProject.Storage
{
    public interface IRepository<T>
    {
        public IQueryable<T> AsQueryable();
        public T Create(T model);
        public T Retrieve(T model);
        public T Delete(T model);
        public T Update(T model);
    }
}
