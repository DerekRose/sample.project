﻿using STC_TestProject.Models;
using STC_TestProject.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_TestProject.Storage
{
    public class OrganizationRepo : IRepository<Organization>
    {
        //mock data
        public List<Organization> _data;

        public OrganizationRepo()
        {
            _data = GetSeedData();
        }

        public static Guid GetDefaultOrgId()
        {
            return new Guid(Resources.DefaultOrgId);
        }

        public static List<Organization> GetSeedData()
        {
            return new List<Organization>
            {
                new Organization
                {
                    Id = Guid.NewGuid(),
                    Name = "Wrong Org"
                },
                new Organization
                {
                    Id = GetDefaultOrgId(),
                    Name = "Default Org"
                },
                new Organization
                {
                    Id = Guid.NewGuid(),
                    Name = "Wrong Org"
                }
            };
        }

        public IQueryable<Organization> AsQueryable()
        {
            return _data.Select(p => (Organization)p).AsQueryable();
        }

        public Organization Create(Organization model)
        {
            throw new NotImplementedException();
        }

        public Organization Delete(Organization model)
        {
            throw new NotImplementedException();
        }

        public Organization Retrieve(Organization model)
        {
            throw new NotImplementedException();
        }

        public Organization Update(Organization model)
        {
            throw new NotImplementedException();
        }
    }
}
