﻿using STC_TestProject.Models;
using STC_TestProject.Properties;
using System;
using System.Collections.Generic;
using System.Linq;

namespace STC_TestProject.Storage
{
    public class ContactRepo : IRepository<ContactInfo>
    {
        //mock data
        public List<ContactInfo> _data;

        public ContactRepo()
        {
            _data = GetSeedData();
        }

        public static List<ContactInfo> GetSeedData()
        {
            return new List<ContactInfo>
            {
                new ContactInfo
                {
                    Id = Guid.NewGuid()
                },
                new ContactInfo
                {
                    OwnerId = OrganizationRepo.GetDefaultOrgId(),
                    Id = Guid.NewGuid(),
                    Address1 = "123 main st",
                    City = "phx",
                    State = "az",
                    Zip = "85123",
                    Phone = "5551234567",
                    Email = "org_email@email.com"
                },
                new ContactInfo
                {
                    Id = Guid.NewGuid(),
                    OwnerId = UserRepo.GetDefaultUserId(),
                    Address1 = "456 7th st",
                    City = "phx",
                    State = "az",
                    Zip = "85789",
                    Phone = "5552468101",
                    Email = "user_email@email.com"
                }
            };
        }

        public IQueryable<ContactInfo> AsQueryable()
        {
            throw new NotImplementedException();
        }

        public ContactInfo Create(ContactInfo model)
        {
            throw new NotImplementedException();
        }

        public ContactInfo Delete(ContactInfo model)
        {
            throw new NotImplementedException();
        }

        public ContactInfo Retrieve(ContactInfo model)
        {
            throw new NotImplementedException();
        }

        public ContactInfo Update(ContactInfo model)
        {
            throw new NotImplementedException();
        }
    }
}
