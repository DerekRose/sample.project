using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_TestProject
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //Fix bug. Endpoint throws an exception
            //http://localhost:5000/api/organizations/7828ceed-f069-40c6-8135-2489cb43b6b2

            //Impliment Create/Retrieve/Update methods in Repositories (Organization, User, ContactInfo)
            //Unit tests should pass

            //Implement Endpoints so that you can create a new org, update, and retrieve it

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
