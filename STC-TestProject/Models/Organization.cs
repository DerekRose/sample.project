﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_TestProject.Models
{
    public class Organization : Resource
    {
        public string Name { get; set; }
        public ContactInfo Contact { get; set; }
        public User AdminUser { get; set; }
    }

    //public class OrganizationViewModel
    //{
    //    public Guid OwnerId { get; set; }
    //    public Guid Id { get; set; }
    //    public string Name { get; set; }
    //    public static implicit operator Organization(OrganizationViewModel model)
    //    {
    //        return new Organization
    //        {
    //            Id = model.Id,
    //            OwnerId = model.OwnerId,
    //            Name = model.Name
    //        };
    //    }

    //    public static implicit operator OrganizationViewModel(Organization model)
    //    {
    //        return new OrganizationViewModel
    //        {
    //            Id = model.Id,
    //            OwnerId = model.OwnerId,
    //            Name = model.Name
    //        };
    //    }
    //}
}
