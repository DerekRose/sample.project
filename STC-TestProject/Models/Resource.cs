﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_TestProject.Models
{
    public class Resource
    {
        public Guid OwnerId { get; set; }
        public Guid Id { get; set; }
    }
}
