﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STC_TestProject.Models
{
    public class User : Resource
    {
        public string Name { get; set; }
        public ContactInfo Contact { get; set; }
    }
}
