﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Simple.Mocking;
using STC_TestProject.Controllers;
using STC_TestProject.Models;
using STC_TestProject.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestProject.Tests
{
    [TestClass]
    public class UpdateTests
    {
        IRepository<Organization> _orgRepo;
        List<Organization> _orgData;

        IRepository<User> _userRepo;
        List<User> _userData;

        IRepository<ContactInfo> _contactRepo;
        List<ContactInfo> _contactData;

        [TestInitialize]
        public void Initialize()
        {
            _orgRepo = Mock.Interface<IRepository<Organization>>();
            _orgData = OrganizationRepo.GetSeedData();
            Expect.MethodCall(() => _orgRepo.Create(Any<Organization>.Value)).Executes(p =>
            {
                //mock repo functionality
                var org = (Organization)p.First();
                var orgToUpdate = _orgData.FirstOrDefault(p => p.Id == org.Id);
                return null;
            });
        }

        [TestMethod]
        public void UpdateOrganization_Success()
        {
            var controller = new APIController(null, _orgRepo, _userRepo, _contactRepo);
            var originalValue = _orgData.FirstOrDefault(p => p.Id == OrganizationRepo.GetDefaultOrgId()).Name;
            var org = new Organization
            {
                Name = "Updated Org Name",
                Id = OrganizationRepo.GetDefaultOrgId(),
                AdminUser = new User 
                {
                 Id = UserRepo.GetDefaultUserId(),
                 OwnerId = OrganizationRepo.GetDefaultOrgId(),
                 Name = "Updated user name"
                }
            };
            Assert.AreNotEqual(originalValue, org.Name);
            var result = controller.Update(OrganizationRepo.GetDefaultOrgId(), org);
            var savedOrg = _orgData.SingleOrDefault(p => p.Id == org.Id);
            Assert.IsNotNull(savedOrg);
            Assert.AreEqual(result.Id, savedOrg.Id);
            Assert.AreEqual(result.Name, savedOrg.Name);

            var savedUser = _userData.SingleOrDefault(p => p.Id == org.AdminUser.Id);
            Assert.IsNotNull(savedUser);
            Assert.IsTrue(result.AdminUser.Id == savedUser.Id);
            Assert.AreEqual(result.AdminUser.Name, savedUser.Name);
        }
    }
}
