using Microsoft.VisualStudio.TestTools.UnitTesting;
using Simple.Mocking;
using STC_TestProject.Controllers;
using STC_TestProject.Models;
using STC_TestProject.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestProject.Tests
{
    [TestClass]
    public class RetrieveTests
    {
        IRepository<Organization> _orgRepo;
        List<Organization> _orgData;

        IRepository<User> _userRepo;
        List<User> _userData;

        IRepository<ContactInfo> _contactRepo;
        List<ContactInfo> _contactData;

        [TestInitialize]
        public void Initialize()
        {
            _orgData = OrganizationRepo.GetSeedData();
            _orgRepo = Mock.Interface<IRepository<Organization>>();
            Expect.MethodCall(() => _orgRepo.AsQueryable()).Returns(_orgData.AsQueryable());


            _userData = UserRepo.GetSeedData();
            _userRepo = Mock.Interface<IRepository<User>>();
            Expect.MethodCall(() => _userRepo.AsQueryable()).Returns(_userData.AsQueryable());

            _contactData = ContactRepo.GetSeedData();
            _contactRepo = Mock.Interface<IRepository<ContactInfo>>();
            Expect.MethodCall(() => _contactRepo.AsQueryable()).Returns(_contactData.AsQueryable());
        }

        [TestMethod]
        public void RetrieveOrganizationById_Success()
        {
            var controller = new APIController(null, _orgRepo, _userRepo, _contactRepo);
            var result = controller.Get(OrganizationRepo.GetDefaultOrgId());
        }
    }
}
