### What is this repository for? ###

* A Basic wireframe service written with a few bugs and some missing code. Fill in the blanks to get it working. 
* Fix bug. This endpoint throws an exception `GET http://localhost:5000/api/organizations/7828ceed-f069-40c6-8135-2489cb43b6b2`
* Impliment Create/Retrieve/Update methods in Repositories (Organization, User, ContactInfo)
* All unit tests should pass
* Implement logic in endpoints so that you can create a new org, update, and retrieve it

### How do I get set up? ###

* Will run out of the box in visual studio
* The "data" is just a List<T> in the repositories, to avoid needing to connect to an actual database

### Contribution guidelines ###

* Create a new branch before making changes

### Who do I talk to? ###

* derek_rose@stchome.com